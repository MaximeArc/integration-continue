import java.util.regex.Pattern;

public class EmailValidator {
    String regexPattern = "^(?=.{2,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
            + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
    public EmailValidator() {
       // Constructor
    }
    public boolean isValid(String email) {
        if(email==null){
            return false;
        }
        return Pattern.compile(regexPattern).matcher(email).matches();
    }
}