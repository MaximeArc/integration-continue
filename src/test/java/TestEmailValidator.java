import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestEmailValidator {
    ContactService contactService = new ContactService();

    @Test
    void shouldInsertValidEmail() {
        contactService.addContact("Test@gmail.com");
    }

    @Test
    void shouldRefuseInvalidEmail(){
        Assertions.assertThrows(RuntimeException.class, () -> contactService.addContact("T@gmail.com"));
    }

    @Test
    void shouldRefuseDoubleEmail() {
        contactService.addContact("try@gmail.com");
        Assertions.assertThrows(RuntimeException.class, () -> contactService.addContact("try@gmail.com"));
    }
}